

[7m╭────────────────────────────────────────────────────────────────────────────╮[0m
[7m│[0m[7m                                                                            [0m[7m│[0m
[7m│[0m[7m [0m[7mDocument Info for linting_rules_report.pdf[0m[7m                                [0m[7m [0m[7m│[0m
[7m│[0m[7m                                                                            [0m[7m│[0m
[7m╰────────────────────────────────────────────────────────────────────────────╯[0m

None

[38;5;241m linting_rules_report.pdf Bytes Info                                         [0m
┏━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃[1m [0m[1mSize  [0m[1m [0m┃[1;96m [0m[1;96m1,941.3[0m[1;37m kb [0m[1;37m([0m[1;36m1,987,855[0m[1;37m bytes[0m[1;37m)[0m[1;37m                                    [0m[1;96m [0m┃
┡━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ MD5    │[38;5;172m [0m[38;5;172mF9BD539D60946CDEB5B6526F046BD132                                [0m[38;5;172m [0m│
│ SHA1   │[38;5;172m [0m[38;5;172m8601FB1850C31744952C2599CFA86EE8DB6C848B                        [0m[38;5;172m [0m│
│ SHA256 │[38;5;172m [0m[38;5;172m65694217ECF0E27F865415F9408F54579138F23FDBBE75740391AF61B6551954[0m[38;5;172m [0m│
└────────┴──────────────────────────────────────────────────────────────────┘

[38;5;241m Embedded Streams                                                             [0m
┏━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃[1m [0m[1mStream Length[0m[1m [0m┃[1m [0m[1mNode                                                      [0m[1m [0m┃
┡━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ [36m  2,233[0m[37m bytes[0m │ [37m<[0m[97m2[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0]/Ki…[0m │
│ [36m 25,639[0m[37m bytes[0m │ [37m<[0m[97m4[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0]/Ki…[0m │
│ [36m 25,311[0m[37m bytes[0m │ [37m<[0m[97m6[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0]/Ki…[0m │
│ [36m 14,470[0m[37m bytes[0m │ [37m<[0m[97m8[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1]/Ki…[0m │
│ [36m 23,006[0m[37m bytes[0m │ [37m<[0m[97m10[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1]/K…[0m │
│ [36m 25,623[0m[37m bytes[0m │ [37m<[0m[97m12[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1]/K…[0m │
│ [36m 22,985[0m[37m bytes[0m │ [37m<[0m[97m14[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1]/K…[0m │
│ [36m 23,821[0m[37m bytes[0m │ [37m<[0m[97m16[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1]/K…[0m │
│ [36m 24,327[0m[37m bytes[0m │ [37m<[0m[97m18[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[2]/K…[0m │
│ [36m 17,873[0m[37m bytes[0m │ [37m<[0m[97m20[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[2]/K…[0m │
│ [36m 26,042[0m[37m bytes[0m │ [37m<[0m[97m22[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[2]/K…[0m │
│ [36m 27,406[0m[37m bytes[0m │ [37m<[0m[97m24[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[2]/K…[0m │
│ [36m 27,257[0m[37m bytes[0m │ [37m<[0m[97m26[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[3]/K…[0m │
│ [36m 26,664[0m[37m bytes[0m │ [37m<[0m[97m28[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[3]/K…[0m │
│ [36m 27,946[0m[37m bytes[0m │ [37m<[0m[97m30[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[3]/K…[0m │
│ [36m 28,730[0m[37m bytes[0m │ [37m<[0m[97m32[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[3]/K…[0m │
│ [36m 27,679[0m[37m bytes[0m │ [37m<[0m[97m34[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[3]/K…[0m │
│ [36m 27,126[0m[37m bytes[0m │ [37m<[0m[97m36[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[0]/K…[0m │
│ [36m 26,277[0m[37m bytes[0m │ [37m<[0m[97m38[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[0]/K…[0m │
│ [36m 27,003[0m[37m bytes[0m │ [37m<[0m[97m40[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[0]/K…[0m │
│ [36m 19,773[0m[37m bytes[0m │ [37m<[0m[97m42[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[0]/K…[0m │
│ [36m 19,123[0m[37m bytes[0m │ [37m<[0m[97m44[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[1]/K…[0m │
│ [36m 22,066[0m[37m bytes[0m │ [37m<[0m[97m46[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[1]/K…[0m │
│ [36m 26,003[0m[37m bytes[0m │ [37m<[0m[97m48[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[1]/K…[0m │
│ [36m 25,588[0m[37m bytes[0m │ [37m<[0m[97m50[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[1]/K…[0m │
│ [36m 27,606[0m[37m bytes[0m │ [37m<[0m[97m52[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[1]/K…[0m │
│ [36m 20,432[0m[37m bytes[0m │ [37m<[0m[97m54[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[2]/K…[0m │
│ [36m 28,242[0m[37m bytes[0m │ [37m<[0m[97m56[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[2]/K…[0m │
│ [36m 25,545[0m[37m bytes[0m │ [37m<[0m[97m58[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[2]/K…[0m │
│ [36m 24,309[0m[37m bytes[0m │ [37m<[0m[97m60[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[2]/K…[0m │
│ [36m 22,183[0m[37m bytes[0m │ [37m<[0m[97m62[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[2]/K…[0m │
│ [36m 21,197[0m[37m bytes[0m │ [37m<[0m[97m64[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[3]/K…[0m │
│ [36m 25,033[0m[37m bytes[0m │ [37m<[0m[97m66[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[3]/K…[0m │
│ [36m 26,760[0m[37m bytes[0m │ [37m<[0m[97m68[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[3]/K…[0m │
│ [36m 27,611[0m[37m bytes[0m │ [37m<[0m[97m70[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[3]/K…[0m │
│ [36m 21,353[0m[37m bytes[0m │ [37m<[0m[97m72[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[1]/Kids[3]/K…[0m │
│ [36m 22,348[0m[37m bytes[0m │ [37m<[0m[97m74[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[0]/K…[0m │
│ [36m 23,417[0m[37m bytes[0m │ [37m<[0m[97m76[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[0]/K…[0m │
│ [36m 26,848[0m[37m bytes[0m │ [37m<[0m[97m78[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[0]/K…[0m │
│ [36m 27,025[0m[37m bytes[0m │ [37m<[0m[97m80[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[0]/K…[0m │
│ [36m 26,171[0m[37m bytes[0m │ [37m<[0m[97m82[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[1]/K…[0m │
│ [36m 26,851[0m[37m bytes[0m │ [37m<[0m[97m84[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[1]/K…[0m │
│ [36m 13,483[0m[37m bytes[0m │ [37m<[0m[97m86[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[1]/K…[0m │
│ [36m 26,088[0m[37m bytes[0m │ [37m<[0m[97m88[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[1]/K…[0m │
│ [36m 26,901[0m[37m bytes[0m │ [37m<[0m[97m90[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[1]/K…[0m │
│ [36m 26,339[0m[37m bytes[0m │ [37m<[0m[97m92[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[2]/K…[0m │
│ [36m 26,522[0m[37m bytes[0m │ [37m<[0m[97m94[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[2]/K…[0m │
│ [36m 27,961[0m[37m bytes[0m │ [37m<[0m[97m96[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[2]/K…[0m │
│ [36m 26,498[0m[37m bytes[0m │ [37m<[0m[97m98[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[2]/K…[0m │
│ [36m 25,846[0m[37m bytes[0m │ [37m<[0m[97m100[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[2]/…[0m │
│ [36m 28,190[0m[37m bytes[0m │ [37m<[0m[97m102[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[3]/…[0m │
│ [36m 26,649[0m[37m bytes[0m │ [37m<[0m[97m104[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[3]/…[0m │
│ [36m 27,045[0m[37m bytes[0m │ [37m<[0m[97m106[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[3]/…[0m │
│ [36m 27,273[0m[37m bytes[0m │ [37m<[0m[97m108[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[3]/…[0m │
│ [36m 26,579[0m[37m bytes[0m │ [37m<[0m[97m110[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[2]/Kids[3]/…[0m │
│ [36m 26,864[0m[37m bytes[0m │ [37m<[0m[97m112[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[0]/…[0m │
│ [36m 26,811[0m[37m bytes[0m │ [37m<[0m[97m114[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[0]/…[0m │
│ [36m 29,039[0m[37m bytes[0m │ [37m<[0m[97m116[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[0]/…[0m │
│ [36m 26,937[0m[37m bytes[0m │ [37m<[0m[97m118[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[0]/…[0m │
│ [36m 28,033[0m[37m bytes[0m │ [37m<[0m[97m120[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[1]/…[0m │
│ [36m 27,714[0m[37m bytes[0m │ [37m<[0m[97m122[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[1]/…[0m │
│ [36m 26,683[0m[37m bytes[0m │ [37m<[0m[97m124[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[1]/…[0m │
│ [36m 26,351[0m[37m bytes[0m │ [37m<[0m[97m126[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[1]/…[0m │
│ [36m  7,392[0m[37m bytes[0m │ [37m<[0m[97m128[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[1]/…[0m │
│ [36m 17,352[0m[37m bytes[0m │ [37m<[0m[97m130[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[2]/…[0m │
│ [36m 19,042[0m[37m bytes[0m │ [37m<[0m[97m132[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[2]/…[0m │
│ [36m 18,557[0m[37m bytes[0m │ [37m<[0m[97m134[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[2]/…[0m │
│ [36m 18,894[0m[37m bytes[0m │ [37m<[0m[97m136[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[2]/…[0m │
│ [36m  2,799[0m[37m bytes[0m │ [37m<[0m[97m138[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[2]/…[0m │
│ [36m 21,015[0m[37m bytes[0m │ [37m<[0m[97m140[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[3]/…[0m │
│ [36m 22,681[0m[37m bytes[0m │ [37m<[0m[97m142[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[3]/…[0m │
│ [36m  5,736[0m[37m bytes[0m │ [37m<[0m[97m144[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[3]/…[0m │
│ [36m  7,243[0m[37m bytes[0m │ [37m<[0m[97m146[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[3]/…[0m │
│ [36m     -1[0m[37m bytes[0m │ [37m<[0m[97m148[0m[37m:[0m[1;4;38;5;59mXObject:Image[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kid…[0m │
│ [36m  1,052[0m[37m bytes[0m │ [37m<[0m[97m149[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[3]/Kids[3]/…[0m │
│ [36m     -1[0m[37m bytes[0m │ [37m<[0m[97m150[0m[37m:[0m[1;4;38;5;59mXObject:Image[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kid…[0m │
│ [36m    694[0m[37m bytes[0m │ [37m<[0m[97m151[0m[37m:[0m[1;4;38;5;35mMetadata:XML[0m[37m([0m[3;38;5;214mDecodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids…[0m │
│ [36m     -1[0m[37m bytes[0m │ [37m<[0m[97m152[0m[37m:[0m[1;4;38;5;59mXObject:Image[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kid…[0m │
│ [36m    760[0m[37m bytes[0m │ [37m<[0m[97m163[0m[37m:[0m[1;4;38;5;239mToUnicode[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1]…[0m │
│ [36m 20,646[0m[37m bytes[0m │ [37m<[0m[97m164[0m[37m:[0m[1;4;38;5;81mFontFile3[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m...s/Kids[0]/Kids[1]/Kids[3…[0m │
│ [36m    143[0m[37m bytes[0m │ [37m<[0m[97m165[0m[37m:[0m[1;4;33mCIDSet[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m...s/Kids[0]/Kids[1]/Kids[3]/K…[0m │
│ [36m     -1[0m[37m bytes[0m │ [37m<[0m[97m2172[0m[37m:[0m[1;4;38;5;59mXObject:Image[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Ki…[0m │
│ [36m    706[0m[37m bytes[0m │ [37m<[0m[97m2173[0m[37m:[0m[1;4;38;5;141mContents[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0]…[0m │
│ [36m     -1[0m[37m bytes[0m │ [37m<[0m[97m2174[0m[37m:[0m[1;4;38;5;59mXObject:Image[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Ki…[0m │
│ [36m    805[0m[37m bytes[0m │ [37m<[0m[97m2177[0m[37m:[0m[1;4;38;5;239mToUnicode[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[1…[0m │
│ [36m    725[0m[37m bytes[0m │ [37m<[0m[97m2178[0m[37m:[0m[1;4;38;5;239mToUnicode[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0…[0m │
│ [36m 10,309[0m[37m bytes[0m │ [37m<[0m[97m2179[0m[37m:[0m[1;4;38;5;81mFontFile3[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m...s/Kids[0]/Kids[1]/Kids[…[0m │
│ [36m    143[0m[37m bytes[0m │ [37m<[0m[97m2180[0m[37m:[0m[1;4;33mCIDSet[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m...s/Kids[0]/Kids[1]/Kids[3]/…[0m │
│ [36m  8,996[0m[37m bytes[0m │ [37m<[0m[97m2181[0m[37m:[0m[1;4;38;5;81mFontFile3[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0…[0m │
│ [36m    143[0m[37m bytes[0m │ [37m<[0m[97m2182[0m[37m:[0m[1;4;33mCIDSet[0m[37m([0m[2;3;38;5;100mEncodedStream[0m[37m)[0m[97m@[0m[38;5;238m/Root/Pages/Kids[0]/Kids[0]/K…[0m │
└───────────────┴────────────────────────────────────────────────────────────┘

